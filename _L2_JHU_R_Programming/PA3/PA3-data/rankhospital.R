rankhospital <- function(state = character(), outcome_name = character(), num = "best") {
      # This function will find the outcome_name in the relevant columns.
      findSicknessCol <- function(df, sickness) {
        # Make all of the column names lower case
        lower_names <- unlist(lapply(names(df), "tolower"))
        
        # Clean the illness argument in case of '.', '_', ' ' between two words
        if ( length(grep(" ", sickness, fixed=TRUE)) != 0 ){
            sickness <- strsplit(sickness, " ", fixed = TRUE)
        } else if (length(grep(".", sickness, fixed=TRUE)) != 0) {
            sickness <- strsplit(sickness, ".", fixed = TRUE)
        } else if (length(grep("_", sickness, fixed=TRUE)) != 0) {
            sickness <- strsplit(sickness, "_", fixed = TRUE)
        } else if (sickness == "heartattack") {
            sickness <- c("heart", "attack")
        } else if (sickness == "heartfailure") {
            sickness <- c("heart", "failure")
        } 
        sickness <- unlist(sickness)
        
        # Narrow down the desired column out of 46 available
        m_hos <- grepl("hospital",lower_names)
        m_30  <- grepl("30", lower_names)
        m_foot <- grepl("footnote", lower_names) ## NOT
        m_comp <- grepl("comparison", lower_names) ## NOT
        m_read <- grepl("readmission", lower_names) ## NOT
        m_pats <- grepl("patients", lower_names) ## NOT
        m_est <- grepl("estimate", lower_names) ## NOT
        m_subtotal <- ( m_hos & m_30    & !m_foot & !m_comp 
                              & !m_read & !m_pats & !m_est  )
        
        # Use two different conditions, depending on 1-word 'pneumonia'
        #     and 2-word 'heart attack' or 'heart failure'
        if (length(sickness) == 1) {
          m_total <- (m_subtotal & grepl(sickness, lower_names))
        } else {
          m_total <- (m_subtotal & grepl(sickness[2], lower_names))
        }
        
        # Successfully return the column that we want to search values of.
        grep(TRUE,m_total)
      }
      
      # This function will return a subset dataframe of the state requested.
      stateReduce <- function(df, user_state) {
        user_state <- toupper(user_state)
        state_rows_bool <- (df[,7] == user_state)
        df[state_rows_bool,]
      }
      
      outcomeVerify <- function(illness) {
        illness<-tolower(illness)
        acceptable = c("pneumonia", "heart attack", "heart_attack", "heart.attack",
                       "heart failure", "heart_failure", "heart.failure")
        if(sum(grepl(illness, acceptable)) < 1) {
          stop("invalid outcome")
        }
      }
      
      stateVerify <- function(user_state) {
          state_array = c("AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA",
                          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
                          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
                          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY")
          if (sum(grepl(toupper(user_state), state_array)) != 1) {
            stop("invalid state")
          }
      }
      
      outcomeVerify(outcome_name)         ## checks that illness is valid
      stateVerify(state)                  ## checks that state is valid
      outcome_vanilla_DF <- read.csv("outcome-of-care-measures.csv",
                                      stringsAsFactors = FALSE)     ## assigns csv datafile to dataframe
      outcome_name <- tolower(outcome_name)   ## illness in lower caps
      sick_col <- findSicknessCol(outcome_vanilla_DF, outcome_name)  ## returns column number of illness
      df_outcome_state_specific <- stateReduce(outcome_vanilla_DF, state) ## subset of 1 state
#      print(df_outcome_state_specific[,sick_col])
      non_NA <- df_outcome_state_specific[,sick_col] != "Not Available"   ## boolean vector of non-NAs

      ## sort mortality lowest to highest
      sorted_mortality <- sort(as.numeric(df_outcome_state_specific[non_NA,sick_col]))
      
      if ( (num > length(sorted_mortality)) & (typeof(num) == typeof(1)) ) {
        return(NA)
      }
      
      if (is.character(num)) {   ## if 'num' is a string, converts it to appropriate numeric
        num <- tolower(num)
        if (num == "best") {
            num <- 1
        } else if (num == "worst") {
            num <- length(sorted_mortality)
            print(num)
        } else {
            stop('invalid ranking')
        }
      }

      
      desired_mortality <- sorted_mortality[num] ## assigns the desired rank morality to variable
      
      if ( sum(grepl(desired_mortality, sorted_mortality)) > 1) {
        ### script when the ranking is tied.
        
      }
      
      df_non_NA <- df_outcome_state_specific[non_NA,]  ## dataframe of non-NA rows of the selected state.
      #View(df_non_NA[,c(2,sick_col)])
      a<-order(as.numeric(df_non_NA[,sick_col]))
      #View(df_non_NA[a,c(2,sick_col)])
      target_mort_bool <- as.numeric(df_non_NA[,sick_col]) == desired_mortality ## bool vec of target mort.
      if (sum(target_mort_bool) > 1) {
        print("We have a tie!!!!")
        ties <- sum(target_mort_bool)
        preceding <- (sum(as.numeric(df_non_NA[,sick_col]) < desired_mortality))
        #print(preceding)
        rank_within_subset <- num-preceding
        b_ordered_tied_subset<- sort((df_non_NA[target_mort_bool,2]))
        print(b_ordered_tied_subset[rank_within_subset])
      } else if (sum(target_mort_bool) == 1 ) {
        print("We don't have a tie???")
        print(df_non_NA[target_mort_bool,2])
#        print((target_mort_bool))
#        print((df_non_NA[,2]))
      } else {
        print("error - no matches")
      }

}